<?php

use Cake\Utility\Hash;

require_once __DIR__.'/../vendor/autoload.php';


$rows = [
	['id'=>'01', 'lastName'=>'norimaki', 'firstName'=>'arare', 'gender'=>'female', 'age'=>'18'],
	['id'=>'02', 'lastName'=>'soramame', 'firstName'=>'taro', 'gender'=>'male', 'age'=>'20'],
	['id'=>'03', 'lastName'=>'kimidori', 'firstName'=>'akane', 'gender'=>'female', 'age'=>'18'],
	['id'=>'04', 'lastName'=>'norimaki', 'firstName'=>'senbe', 'gender'=>'male', 'age'=>'32'],
	['id'=>'05', 'lastName'=>'yamabuki', 'firstName'=>'midori', 'gender'=>'female', 'age'=>'27'],
];


echo "firstNameだけ\n";
var_dump(Hash::extract($rows, '{n}.firstName'));

echo "女性だけ\n";
var_dump(Hash::extract($rows, '{n}[gender=female].firstName'));

